#!/usr/bin/bash

# make sure we have rteval installed
if rpm -q --quiet python3-schedutils; then
    :
else
    sudo dnf install -y python3-schedutils
    if [[ $? != 0 ]]; then
	echo "install of python3-schedutils failed!"
	exit 1
    fi
fi

exit 0
